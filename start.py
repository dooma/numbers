__author__ = "Calin Salagean < salagean.calin@gmail.com >"

import tkinter as tk
import tkinter.simpledialog as tkSD
import tkinter.messagebox as tkMB
from algorithm import add, alter, filter, functions, search

class Numbers:
    def __init__(self, master):
        # Current list
        self.current_list = tk.StringVar()
        self.current_list.set(str(self.get_initial_state()))

        # Unique value
        self.uniq_value = tk.IntVar()

        # Main frame for this application
        self.main_frame = tk.Frame(master)
        self.main_frame.pack(fill=tk.BOTH, expand=True)

        # Toolbar
        toolbar = tk.Menu(master)
        master.config(menu=toolbar)
        toolbar.add_command(label="Undo", command=self.pop_undo)

        # Top frame with list
        self.top = tk.Frame(self.main_frame)
        self.top.pack(expand=True)

        self.label = tk.Label(self.top, text="Current list:")
        self.label.pack(side=tk.LEFT)
        self.status = tk.Message(self.top, textvariable=self.current_list, width=1000)
        self.status.pack(fill=tk.X)

        # Buttons frame with list
        self.bottom = tk.Frame(self.main_frame)
        self.bottom.pack(anchor=tk.S, expand=False)

        # Add frame
        self.add = tk.Frame(self.bottom)
        self.add.pack(side=tk.LEFT)

        # Alter frame
        self.alter = tk.Frame(self.bottom)
        self.alter.pack(side=tk.LEFT)

        # Search frame
        self.search = tk.Frame(self.bottom)
        self.search.pack(side=tk.LEFT)

        # Functions frame
        self.functions = tk.Frame(self.bottom)
        self.functions.pack(side=tk.LEFT)

        # Filter frame
        self.filter = tk.Frame(self.bottom)
        self.filter.pack(side=tk.LEFT)

        self.append = tk.Button(self.add, text="Append element", command=self.append_dialog)
        self.append.pack(fill=tk.X)

        self.insert = tk.Button(self.add, text="Insert element", command=self.insert_dialog)
        self.insert.pack(fill=tk.X)

        self.delete = tk.Button(self.alter, text="Delete element", command=self.delete_dialog)
        self.delete.pack(fill=tk.X)

        self.delete_list = tk.Button(self.alter, text="Delete a list", command=self.delete_list_dialog)
        self.delete_list.pack(fill=tk.X)

        self.replace = tk.Button(self.alter, text="Replace elements", command=self.replace_elements_dialog)
        self.replace.pack(fill=tk.X)

        self.primes = tk.Button(self.search, text="List primes numbers", command=self.list_primes_dialog)
        self.primes.pack(fill=tk.X)

        self.evens = tk.Button(self.search, text="List even numbers", command=self.list_evens_dialog)
        self.evens.pack(fill=tk.X)

        self.sum = tk.Button(self.functions, text="Make sum", command=self.get_sum)
        self.sum.pack(fill=tk.X)

        self.max = tk.Button(self.functions, text="Maximum number", command=self.get_max)
        self.max.pack(fill=tk.X)

        self.nonprime = tk.Button(self.filter, text="Remove nonprime", command=self.remove_nonprime)
        self.nonprime.pack(fill=tk.X)

        self.negative = tk.Button(self.filter, text="Remove positive", command=self.remove_positive)
        self.negative.pack(fill=tk.X)

    def get_initial_state(self):
        return [22, 85, 11, 13, 26, 39, 33, 31, 87]

    def push_undo(self):
        list = eval(self.current_list.get())
        try:
            self.undo_stack.append(list)
        except AttributeError:
            self.undo_stack = []
            self.undo_stack.append(list)

    def pop_undo(self):
        try:
            list = self.undo_stack.pop()
        except (IndexError, AttributeError) as exception:
            list = self.get_initial_state()

        self.current_list.set(str(list))


    def append_dialog(self):
        self.push_undo()
        element = tkSD.askstring("Append element", "Please enter an element")
        list = eval(self.current_list.get())
        try:
            element = int(element)
            list = add.append(list, element)
        except ValueError:
            elements = element.split(",")
            for element in elements:
                try:
                    element = int(element)
                    list = add.append(list, int(element))
                except TypeError:
                    pass
        self.current_list.set(str(list))

    def insert_dialog(self):
        self.push_undo()
        element = tkSD.askinteger("Insert element", "Please enter an element")
        index = tkSD.askinteger("Index", "Please enter the position")
        list = eval(self.current_list.get())

        if element != None:
            list = add.append(list, element, index)

        self.current_list.set(str(list))

    def delete_dialog(self):
        self.push_undo()
        index = tkSD.askinteger("Delete element", "Please enter the position")
        list = eval(self.current_list.get())
        list = alter.delete(list,index)
        self.current_list.set(str(list))

    def delete_list_dialog(self):
        self.push_undo()
        first_index = tkSD.askinteger("First index", "Please insert first index")
        last_index = tkSD.askinteger("Last index", "Please insert last index")
        list = eval(self.current_list.get())

        if last_index is None:
            last_index = len(list)

        list = alter.delete_list(list, first_index, last_index)
        self.current_list.set(str(list))

    def replace_elements_dialog(self):
        self.push_undo()
        index = tkSD.askinteger("Index", "Please enter index of element")
        element = tkSD.askinteger("Element", "Please enter the value that you want inserted")
        list = eval(self.current_list.get())
        list = alter.change(list, index, element)
        self.current_list.set(str(list))

    def list_primes_dialog(self):
        list = eval(self.current_list.get())
        list = search.primes(list)
        tkMB.showinfo("Prime numbers", str(list))

    def list_evens_dialog(self):
        list = eval(self.current_list.get())
        list = search.evens(list)
        tkMB.showinfo("Even numbers", str(list))

    def get_sum(self):
        list = eval(self.current_list.get())
        theSum = functions.sum_list(list)
        tkMB.showinfo("Sum of the elements", "The sum is: " + str(theSum))

    def get_max(self):
        list = eval(self.current_list.get())
        theMax = functions.max_list(list)

        if theMax == None:
            message = "There is no maximum value in the list"
        else:
            message = "The maximum number is: " + str(theMax)

        tkMB.showinfo("Maximum number", message)

    def remove_nonprime(self):
        self.push_undo()
        list = eval(self.current_list.get())
        list = filter.remove_nonprime_numbers(list)
        self.current_list.set(str(list))

    def remove_positive(self):
        self.push_undo()
        list = eval(self.current_list.get())
        list = filter.remove_positive_numbers(list)
        self.current_list.set(str(list))

root = tk.Tk()
root.title("Numbers")
root.geometry("640x240")
numbers = Numbers(root)
root.mainloop()