from distutils.core import setup

setup(
    name='numbers',
    version='0.0.1',
    packages=[''],
    url='https://bitbucket.org/dooma/numbers',
    license='',
    author='Calin Salagean',
    author_email='salagean.calin@gmail.com',
    description='A simple application that helps childs to learn numbers properties.'
)
