"""
Test ALTER module

Author: Calin Salagean < salagean.calin@gmail.com >
"""

from algorithm import alter


def test_delete():
    assert(alter.delete([], 0) == [])
    assert(alter.delete([], 1) == [])
    assert(alter.delete([1,2,3], 1) == [1,3])
    assert(alter.delete([1,2,3], 5) == [1,2,3])

def test_delete_list():
    assert(alter.delete_list([], 0, 1) == [])
    assert(alter.delete_list([1,2,3], 1, 2) == [1])
    assert(alter.delete_list([1,2,3], 3, 4) == [1,2,3])
    assert(alter.delete_list([1,2,3], 7, 6) == [1,2,3])

def test_change():
    assert(alter.change([], 1, 6) == [])
    assert(alter.change([1,2,3,4], 2, 6) == [1,2,6,4])
    assert(alter.change([1,2,3], 2, 6) == [1,2,6])

test_delete()
test_delete_list()
test_change()
