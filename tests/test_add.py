"""
Test ADD module

Author: Calin Salagean < salagean.calin@gmail.com >
"""
from algorithm import add


def test_append():
    assert(add.append([], 1) == [1])
    assert(add.append([1,2], 0, 0) == [0,1,2])
    assert(add.append([1,2], 3) == [1,2,3])

test_append()
