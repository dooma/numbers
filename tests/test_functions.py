"""
Test FUNCTIONS module

Author: Calin Salagean < salagean.calin@gmail.com >
"""

from algorithm import functions


def test_sum():
    assert(functions.sum_list([]) == 0)
    assert(functions.sum_list([1,2,3,4,5]) == 15)
    assert(functions.sum_list([1,2,3,4,5],1) == 14)
    assert(functions.sum_list([1,2,3,4,5], 1, 2) == 5)
    assert(functions.sum_list([1,2,3,4,5], 1, 10) == 14)

def test_max():
    assert(functions.max_list([]) == None)
    assert(functions.max_list([1]) == 1)
    assert(functions.max_list([1,2,3,4,5]) == 5)
    assert(functions.max_list([-2,-3,0,1,2], 0, 1) == -2)
    assert(functions.max_list([-2,-3,-4,-1], 2) == -1)
    assert(functions.max_list([-2,-3,-4,-1], 1, 100) == -1)
    assert(functions.max_list([-2,-3,-4,-1], -1, 100) == -1)

def test_cmmdc():
    assert(functions.cmmdc(1, 2) == 1)
    assert(functions.cmmdc(2, 4) == 2)
    assert(functions.cmmdc(-1, 2) == -1)
    assert(functions.cmmdc(4.2, 3) == None)
    assert(functions.cmmdc(4.2, 3.1) == None)

def test_cmmdc_list():
    assert(functions.cmmdc_list([1,2,4,5,6]) == 1)
    assert(functions.cmmdc_list([]) == None)
    assert(functions.cmmdc_list([], 2, 3) == None)
    assert(functions.cmmdc_list([6,10]) == 2)
    assert(functions.cmmdc_list([2,4,24], 1) == 4)
    assert(functions.cmmdc_list([2,4,24], 1, 1) == 4)

def test_sort():
    assert(functions.sort([]) == [])
    assert(functions.sort([1,2,3,4,5]) == [5,4,3,2,1])
    assert(functions.sort([1,2,3,4,5], 1) == [5,4,3,2])
    assert(functions.sort([1,2,3,4,5], 1, 3) == [4,3,2])

test_sum()
test_max()
test_cmmdc()
test_cmmdc_list()
test_sort()