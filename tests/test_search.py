"""
Test SEARCH module

Author: Calin Salagean < salagean.calin@gmail.com >
"""

from algorithm import search


def test_is_prime():
    assert(search.is_prime(1) == False)
    assert(search.is_prime(-1) == False)
    assert(search.is_prime(2) == True)
    assert(search.is_prime(4) == False)
    assert(search.is_prime(5) == True)

def test_primes():
    assert(search.primes([]) == [])
    assert(search.primes([1, 5, 4, 3, 8, 7]) == [5, 3, 7])
    assert(search.primes([1, 5, 4, 3, 8, 7], 1, 3) == [5, 3])
    assert(search.primes([1, 5, 4, 3, 8, 7], 1, 8) == [5, 3, 7])

def test_evens():
    assert(search.evens([]) == [])
    assert(search.evens([1,2]) == [2])
    assert(search.evens([1,2,3,4,5,6], 1) == [2,4,6])
    assert(search.evens([1,2,3,4,5,6], 3, 10) == [4,6])

test_is_prime()
test_primes()
test_evens()
