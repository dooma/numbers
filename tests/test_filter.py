"""
Test FILTER module

Author: Calin Salagean < salagean.calin@gmail.com >
"""

from algorithm import filter


def test_remove_nonprime_numbers():
    assert(filter.remove_nonprime_numbers([]) == [])
    assert(filter.remove_nonprime_numbers([1, 5, 4, 3, 8, 7]) == [5, 3, 7])
    assert(filter.remove_nonprime_numbers([1, 5, 4, 3, 8, 7], 1, 3) == [5, 3])
    assert(filter.remove_nonprime_numbers([1, 5, 4, 3, 8, 7], 1, 8) == [5, 3, 7])

def test_remove_positive_numbers():
    assert(filter.remove_positive_numbers([]) == [])
    assert(filter.remove_positive_numbers([1, 2, 3, 4, -1, -4]) == [-1, -4])
    assert(filter.remove_positive_numbers([-2, -5, -8, 0], 1) == [-5, -8])
    assert(filter.remove_positive_numbers([-2, -5, -8, 0], 1, 1) == [-5])

test_remove_nonprime_numbers()
test_remove_positive_numbers()