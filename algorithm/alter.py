"""
Library module that contains methods to alter elements in a list

Author: Calin Salagean < salagean.calin@gmail.com >
"""

def delete(array, index):
    """
    This method should delete an element on a specified position(number, array)
    Parameters:
        - array (list)
        - index (integer)
    Return:
        - list
    """

    try:
        array.pop(index)
    except IndexError:
        return array

    return array

def delete_list(array, start, end):
    """
    This method should delete an element on a specified position(number, array)
    START parameter should be smaller than END parameter
    Parameters:
        - array (list)
        - start (list)
        - end (integer)
    Return:
        - list
    """

    if start > end:
        start, end = end, start

    for index in range(end, start - 1, -1):
        try:
            array.pop(index)
        except IndexError:
            return array

    return array

def change(array, index, element):
    """
    This method should change an element on a specified position
    Parameters:
        - list
        - integer - index
        - integer - element
    Return:
        - list
    """

    try:
        array.pop(index)
        array.insert(index, element)
    except IndexError:
        return array

    return array
