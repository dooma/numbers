"""
Library module that contains methods to return a sum or a maximum number

Author: Calin Salagean < salagean.calin@gmail.com >
"""

def sum_list(array, start=0, end=-1):
    """
    This method returns the sum of the elements to list provided
    If START and END parameters are specified, it will return the sum of the sublist
    Parameters:
        - array (list)
        - start (integer)
        - end (integer)
    Returns:
        - integer
    """

    if end == -1:
        end += len(array)

    return sum(array[start:end + 1])

def max_list(array, start=0, end=-1):
    """
    This method returns the maximum element of the list
    If START and END parameters are specified, it will return the maximum number of the sublist
    Parameters:
        - array (list)
        - start (integer)
        - end (integer)
    Returns:
        - integer or None if the list is empty
    """

    if end == -1:
        end += len(array)

    try:
        return max(array[start:end + 1])
    except  ValueError:
        return None

def cmmdc(first, second):
    """
    This method return the biggest divisor of two numbers
    Parameters:
        - first (integer)
        - second (integer)
    Returns:
        - integer or None if numbers are not integers
    """

    if not type(first) is int or not type(second) is int:
        return None

    if not first or not second:
        return first + second
    elif first > second:
        return cmmdc(first%second, second)
    return cmmdc(first, second%first)

def cmmdc_list(array, start=0, end=-1):
    """
    This method returns the biggest divisor of a list of numbers
    If START and END parameters are specified, it will return the biggest divisor of the sublist
    Parameters:
        - array (list)
        - start (integer)
        - end (integer)
    Returns:
        - integer or None if the list is empty
    """

    if end == -1:
        end += len(array)

    try:
        bigDiv = array[start]
    except:
        return None

    for elem in array[start + 1:end + 1]:
        bigDiv = cmmdc(bigDiv, elem)

    return bigDiv

def sort(array, start=0, end=-1):
    """
    This method returns a sorted list in descending order
    Parameters:
        - array (list)
        - start (integer)
        - end (integer)
    Returns:
        - list
    """

    if end == -1:
        end += len(array)

    return sorted(array[start:end+1], reverse=True)