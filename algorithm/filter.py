"""
Library module that contains methods to remove elements from an array

Author: Calin Salagean < salagean.calin@gmail.com >
"""

import algorithm.search as search

def remove_nonprime_numbers(array, start=0, end=-1):
    """
    This method return a list with prime numbers
    If START and END parameters are specified, the method will return prime elements from start to end index
    Parameters:
        - array (list)
        - start (integer)
        - end (integer)
    Returns:
        - list
    """

    return search.primes(array, start, end)

def remove_positive_numbers(array, start=0, end=-1):
    """
    This method returns a list with negative numbers from the list
    If START and END parameters are specified, the method will return negative numbers from list
    Parameters:
        - array (list)
        - start (integer)
        - end (integer)
    Returns:
        - list
    """

    if end == -1:
        end += len(array)

    return list(filter(lambda x: x < 0, array[start:end + 1]))