"""
Library module that contains methods to search elements in a list

Author: Calin Salagean < salagean.calin@gmail.com >
"""

def is_prime(number):
    """
    This method checks if a number is prime
    Parameters:
        - number (integer)
    Return:
        - boolean
    """

    if number < 2 or (number > 2 and not number % 2):
        return False

    for div in range(3, int(number**1/2), 2):
        if not number % div:
            return False

    return True

def primes(array, start = 0, end = -1):
    """
    This method returns a list with prime numbers of sent list as parameter
    If START and END parameters are specified, it will return the primes of a sublist
    Parameters:
        - array (list)
        - start (integer)
        - end (integer)
    Returns:
        - list
    """

    if end == -1:
        end += len(array)

    primesArray = []

    for element in array[start:end + 1]:
        if is_prime(element):
            primesArray.append(element)

    return primesArray

def evens(array, start = 0, end = -1):
    """
    This method returns a list with even numbers of sent list as parameter
    If START and END parameters are specified, it will return the primes of a sublist
    Parameters:
        - array (list)
        - start (integer)
        - end (integer)
    Returns:
        - list
    """

    if end == -1:
        end += len(array)

    evensArray = []

    for element in array[start:end + 1]:
        if not element % 2:
            evensArray.append(element)

    return evensArray
