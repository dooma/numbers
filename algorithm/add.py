"""
Library module that contains methods to insert elements into a list

Author: Calin Salagean < salagean.calin@gmail.com >
"""

def append(array, element, index = None):
    """
    This method appends an element to a list
    Parameters:
        - list
        - element
        - index(optional - if it is specified, append into a position)
    Return:
        - list
    """
    if index == None:
        index = len(array)

    array.insert(index, element)

    return array
